package com.bbva.uuaa.dto.banco;

import java.io.Serializable;



/**
 * The PaginationInDTO class...
 */
public class PaginationInDTO implements Serializable  {
	private static final long serialVersionUID = 2931699728946643245L;

	/* Attributes section for the DTO */

	//private para encapsular los datos 
	private int paginationkey;
	private int paginationSize;
	
	
	public int getPaginationkey() {
		return paginationkey;
	}
	public void setPaginationkey(int paginationkey) {
		this.paginationkey = paginationkey;
	}
	public int getPaginationSize() {
		return paginationSize;
	}
	public void setPaginationSize(int paginationSize) {
		this.paginationSize = paginationSize;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + paginationSize;
		result = prime * result + paginationkey;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaginationInDTO other = (PaginationInDTO) obj;
		if (paginationSize != other.paginationSize)
			return false;
		if (paginationkey != other.paginationkey)
			return false;
		return true;
	}
	
	
}
