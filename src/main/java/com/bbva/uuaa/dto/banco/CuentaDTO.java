package com.bbva.uuaa.dto.banco;

import java.io.Serializable;


/**
 * The CuentaDTO class...
 */
public class CuentaDTO implements Serializable  {
	private static final long serialVersionUID = 2931699728946643245L;

	//DECLARACION DE DATOS DE ACUERDO A LOS DATOS DE LA BASE DE DATOS 
	
	long numCuenta;
	private String cdDivisa;
	private String cdTipoCuenta;
	private long importe;
	private String fhAlta;
	public long getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getCdDivisa() {
		return cdDivisa;
	}
	public void setCdDivisa(String cdDivisa) {
		this.cdDivisa = cdDivisa;
	}
	public String getCdTipoCuenta() {
		return cdTipoCuenta;
	}
	public void setCdTipoCuenta(String cdTipoCuenta) {
		this.cdTipoCuenta = cdTipoCuenta;
	}
	public long getImporte() {
		return importe;
	}
	public void setImporte(long importe) {
		this.importe = importe;
	}
	public String getFhAlta() {
		return fhAlta;
	}
	public void setFhAlta(String fhAlta) {
		this.fhAlta = fhAlta;
	}
	@Override
	public String toString() {
		return "CuentaDTO [numCuenta=" + numCuenta + ", cdDivisa=" + cdDivisa + ", cdTipoCuenta=" + cdTipoCuenta
				+ ", importe=" + importe + ", fhAlta=" + fhAlta + "]";
	}
	
	
}
