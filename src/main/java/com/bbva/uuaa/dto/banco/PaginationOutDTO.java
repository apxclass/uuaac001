package com.bbva.uuaa.dto.banco;

import java.io.Serializable;



/**
 * The PaginationOutDTO class...
 */
public class PaginationOutDTO implements Serializable  {
	private static final long serialVersionUID = 2931699728946643245L;

	private int paginationKey;
	private boolean hasMoreData;
	
	
	public int getPaginationKey() {
		return paginationKey;
	}
	public void setPaginationKey(int paginationKey) {
		this.paginationKey = paginationKey;
	}
	public boolean isHasMoreData() {
		return hasMoreData;
	}
	public void setHasMoreData(boolean hasMoreData) {
		this.hasMoreData = hasMoreData;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (hasMoreData ? 1231 : 1237);
		result = prime * result + paginationKey;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaginationOutDTO other = (PaginationOutDTO) obj;
		if (hasMoreData != other.hasMoreData)
			return false;
		if (paginationKey != other.paginationKey)
			return false;
		return true;
	}
	
	
	
	
}
